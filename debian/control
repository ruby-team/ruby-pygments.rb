Source: ruby-pygments.rb
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Per Andersson <avtobiff@gmail.com>,
           Dominique Dumont <dod@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb,
               rake,
               rake-compiler,
               python3-pygments (>= 2.15.1~)
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-pygments.rb.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-pygments.rb
Homepage: https://github.com/pygments/pygments.rb
Testsuite: autopkgtest-pkg-ruby
Rules-Requires-Root: no

Package: ruby-pygments.rb
Architecture: all
Depends: ${misc:Depends},
         ${ruby:Depends},
         ${shlibs:Depends},
         python3:any,
         python3-pygments (>= 2.15.1~),
Description: pygments wrapper for Ruby
 A Ruby wrapper for the Python pygments syntax highlighter.
 .
 pygments.rb works by talking over a simple pipe to a long-lived Python child
 process. This library replaces albino, as well as a version of pygments.rb
 that used an embedded Python interpreter.
 .
 Each Ruby process that runs has its own 'personal Python'; for example, 4
 Unicorn workers will have one Python process each.  If a Python process dies,
 a new one will be spawned on the next pygments.rb request.
